# Dockerized Cs API

A backend API challenge that also required dockerization. Done from scratch use .NET core, C# and Docker.

## Challenge Rules
1.) This challenge must be completed in under 48 hours.

2.) Your code and any assets must be version controlled with Git.Your submission must include a hyperlink to a github.com repository that you have pushed your project to.

3.) The github project should have a README file that includes instructions on anything that would need to be done to install and run your submission.

4.) Your submission must be dockerized.

Optional: If you prefer, you may also dockerize your submission on your own for more points! (*I did)


Submissions that cannot be run will not be graded.

Overview
For this challenge, you will be building a simple API which provides a list of supervisors and accepts a registration to receive notifications from a given supervisor.


Server Specifications
Please build an application that exposes an API that allows an application to retrieve a list of supervisors and submit personal information.

Your application must include the following endpoints:

GET/api/supervisors

When this endpoint is hit, a call is made to GET https://o3m5qixdng.execute-api.us-east-1.amazonaws.com/api/managers to pull down a list of managers.

The endpoint should take the response payload from AWS and format it to return:

A list of supervisor strings formatted as follows:
“jurisdiction - lastName, firstName”

Supervisors within the endpoint response payload should be sorted in alphabetical order first by jurisdiction, then by lastName and firstName.

Numeric jurisdictions should be excluded from the response.

POST /api/submit

The endpoint should expect to be provided the following parameters by means of your preferred content type:

firstName,
lastName,
email,
phoneNumber,
supervisor,

The submitted data above should be printed to the console upon receiving the post request.

If firstName, lastName, or supervisor is not provided, the endpoint should return an error response. These are required parameters.
The user of your endpoint should have the option to leave out email and phoneNumber when they submit without issue. They are not required for submission.


